{ config, options, lib, pkgs, ... }:
let
  noise-suppression-for-voice = pkgs.callPackage ./package.nix { };
  paCfg = config.hardware.pulseaudio;
  cfg = paCfg.noise-suppression-for-voice;
in
{
  options.hardware.pulseaudio.noise-suppression-for-voice = {
    enable = lib.mkEnableOption "enable werman noise suppression plugin";
    package = lib.mkOption {
      type = lib.types.package;
      default = noise-suppression-for-voice;
    };

    source = lib.mkOption {
      type = lib.types.str;
    };
  };

  config.hardware.pulseaudio.extraConfig = lib.mkIf (paCfg.enable && cfg.enable) ''
    load-module module-null-sink sink_name=mic_denoised_out rate=48000
    load-module module-ladspa-sink sink_name=mic_raw_in sink_master=mic_denoised_out label=noise_suppressor_mono plugin=${cfg.package}/lib/ladspa/librnnoise_ladspa.so control=50
    load-module module-loopback source=${cfg.source} sink=mic_raw_in channels=1 source_dont_move=true sink_dont_move=true

    set-default-source mic_denoised_out.monitor
  '';
}
