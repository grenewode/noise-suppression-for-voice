{ lib
, stdenv
, fetchFromGitHub
, cmake
}: stdenv.mkDerivation rec {
  pname = "noise-suppression-for-voice";
  version = "0.91";

  buildInputs = [ cmake ];

  src = fetchFromGitHub {
    owner = "werman";
    repo = pname;

    rev = "v${version}";

    hash = "sha256:11pwisbcks7g0mdgcrrv49v3ci1l6m26bbb7f67xz4pr1hai5dwc";
  };

  cmakeFlags = [ "-Bbuild-x64" "-DCMAKE_BUILD_TYPE=Release" "-DBUILD_VST_PLUGIN=OFF" ];
  preBuild = ''cd build-x64'';
}
